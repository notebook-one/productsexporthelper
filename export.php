<?php
ini_set('max_execution_time', '0');

// Config section
define('BASE_URL', 'https://notebook1.ru/status-check/api/v1');
define('IS_DEBUG', true);
const EXPORT_FILE = 'export.xml';
const SCRIPT_URL = 'https://site.com/asc'; // absolute path to folder contains this file
const PRODUCTS_LIMIT = 1000;
// End config section

if (IS_DEBUG) {
    $startTime = microtime(true);
}
showMessage('Start products catalog downloading...');
downloadToFile(BASE_URL . '/goods/?format=xml&limit='.PRODUCTS_LIMIT.'&filter[shop_enable]=true', 'products.xml');
showMessage('Products catalog received.');
checkCatalogExist();
checkImagesFolderExist();

$simpleXml = simplexml_load_file('products.xml');
$totalProducts = count($simpleXml->Product);
showMessage('Total products in catalog: ' . $totalProducts);
$i = $ii = 1;
foreach ($simpleXml->Product as $product) {
    $productId = intval($product->Id);
    showMessage('Get product ' . $ii . ' of ' . $totalProducts . ' details');
    $productJSON = download(BASE_URL . '/goods/' . $productId); // ?format=xml not implemented, so JSON only :facepalm:
    $productDetails = $arr = json_decode($productJSON, true);
    foreach ($productDetails['good']['Images'] as $element) {
        $imagePath = constructProductImagePath($productId, $i);
        file_put_contents($imagePath, base64_decode($element['img']));
        $product->addChild("IMAGE_" . $i, SCRIPT_URL . '/' . $imagePath);
        ++$i;
    }
    $i = 1;
    if (IS_DEBUG) {
        showMessage('Memory usage: ' . convert(memory_get_usage(true)));
    }
    ++$ii;
}

saveXmlToFile($simpleXml);
removeCatalog();

if (IS_DEBUG) {
    $endTime = microtime(true);
    $diff = round($endTime - $startTime);
    $minutes = floor($diff / 60);
    $seconds = $diff % 60;
    showMessage('Complete ' . $totalProducts . ' products in minutes: ' . $minutes . ' seconds: ' . $seconds);
} else {
    showMessage('Complete!');
}


function showMessage($message)
{
    echo $message . PHP_EOL;
}

function constructProductImagePath($productId, $i)
{
    return 'images/' . $productId . '_' . $i . '.jpg';
}

function download($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 180);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $productXML = curl_exec($ch);
    curl_close($ch);
    return $productXML;
}

function downloadToFile($url, $filename)
{
    $fp = fopen(dirname(__FILE__) . '/' . $filename, 'w+');
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 180);
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}

function checkCatalogExist(){
    if (!file_exists('products.xml')) {
        die('Не удалось открыть файл products.xml');
    }
}

function checkImagesFolderExist(){
    if (!file_exists('images')) {
        showMessage('Images folder not exist, creating...');
        if (!mkdir('images', 0777, true)) {
            die('Create images folder failed.');
        }
    }
}

function saveXmlToFile($simpleXml){
    // simplexml can't format document
    $dom = new DOMDocument("1.0");
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($simpleXml->asXML());
    $dom->save(EXPORT_FILE);
}

function removeCatalog(){
    if (!unlink('products.xml')) {
        showMessage('Remove products.xml file failed. Do it yourself.');
    }
}

function convert($size)
{
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}